#!/usr/bin/python
__author__ = "Austin Powell"
__license__ = "MIT"
__version__ = "0.5"
__maintainer__ = "Austin Powell"
__email__ = "aupowell@stanfordchildrens.org"
__status__ = "Research"

import spacy
from spacy.matcher import Matcher
from spacy.matcher import PhraseMatcher
from spacy.tokens import Span, Token, Doc
from spacy.pipeline import EntityRuler

import neuralcoref

class KeyWordRecognizer(object):
    """ Base class for NLP pipeline. Receiving a set of words and word attribute patterns to create entities based on a label passed to class.
    """

    author = "Austin Powell"

    def __init__(self, nlp, patterns=tuple(), label="KEYWORD"):
        """Initialise the pipeline component. The shared nlp instance is used
        to initialise the matcher with the shared vocab, get the label ID and
        generate Doc objects as phrase match patterns.
        """

        # Set the class attribute to add different entities
        KeyWordRecognizer.name = label

        # Set up the Matcher
        self.matcher = Matcher(nlp.vocab)
        self.matcher.add(label, None, *patterns)

        # Register attribute on the Token. We'll be overwriting this based on
        # the matches, so we're only setting a default value, not a getter.
        Token.set_extension(
            "is_mabx_keyword", default="mbax_keyword", force=True
        )  # forces token defualt

        # Register attributes on Doc and Span via a getter that checks if one of
        #  # the contained tokens is set to is_mabx_keyword == True.
        Doc.set_extension("has_mabx_keyword", getter=self.has_mabx_keyword, force=True)
        Span.set_extension("has_mabx_keyword", getter=self.has_mabx_keyword, force=True)

    def __call__(self, doc):
        """Apply the pipeline component on a Doc object and modify it if matches
        are found. Return the Doc, so it can be processed by the next component
        in the pipeline, if available.
        """
        matches = self.matcher(doc)
        seen_tokens = set()
        new_entities = []
        entities = doc.ents
        for match_id, start, end in matches:
            #    span = Span(doc, start, end, label=match_id)
            #    doc.ents = list(doc.ents) + [span]
            # check for end - 1 here because boundaries are inclusive
            if start not in seen_tokens and end - 1 not in seen_tokens:
                new_entities.append(Span(doc, start, end, label=match_id))
                entities = [
                    e for e in entities if not (e.start < end and e.end > start)
                ]
                seen_tokens.update(range(start, end))

        doc.ents = tuple(entities) + tuple(new_entities)
        return doc

        for span in spans:
            # Iterate over all spans and merge them into one token. This is done
            # after setting the entities – otherwise, it would cause mismatched
            # indices!
            span.merge()
        return doc  # don't forget to return the Doc!

    def has_mabx_keyword(self, tokens):
        """Getter for Doc and Span attributes."""
        return any([t._.get("has_mabx_keyword") for t in tokens])

    def change_pipename(self, pipename):
        """In this function you can access KeyWordRecognizer pipename directly"""
        self.name = pipename





def create_teen_nlp_pipeline():
    """
    Load basic spacy package and add variouse recognizers to it.
    """
    def add_recognizer(label=None, recognizer=None):
        try:
            nlp.remove_pipe(label)
            nlp.add_pipe(recognizer, last=True)
        except:
            nlp.add_pipe(recognizer, last=True)

    greetings = ["hi", "hello", "hey", "helloo", "hellooo", "hi there", "g morining", "gmorning", "good morning", "morning", "good day", "good afternoon", "good evening", "greetings", "greeting", "good to see you", "its good seeing you", "how are you", "how're you", "how are you doing", "how ya doin'", "how ya doin", "how is everything", "how is everything going", "how's everything going", "how is you", "how's you", "how are things", "how're things", "how is it going", "how's it going", "how's it goin'", "how's it goin", "how is life been treating you", "how's life been treating you", "how have you been", "how've you been", "what is up", "what's up", "what is cracking", "what's cracking", "what is good", "what's good", "what is happening", "what's happening", "what is new", "what's new", "what is neww", "g’day", "howdy"]

    
    # Load pre-trained embeddings
    print('Loading NLP Pipeline')
    spacy_embeddings = 'en_core_web_lg'
    try:
        print('Loading fresh Spacy embeddings: {}'.format(spacy_embeddings))
        nlp = spacy.load('en_core_web_lg')
    except TypeError as e:
        print("Wasn't successful in loading Spacy embeddings...")
        print(e)


    ruler = EntityRuler(nlp, overwrite_ents=True)
    ###############################################################
    # patient salutation (singular)
    ###############################################################
    salutation_patterns = []
    for greeting in greetings:
        words_in_greeting = greeting.split()
        if len(words_in_greeting) > 1:
            long_greeting = []
            for word in words_in_greeting:
                long_greeting.append({"LOWER": word})
            long_greeting.append({"TEXT":{"REGEX": "([^\s]+)"}})  # {"ENT_TYPE": "PERSON"}
            salutation_patterns.append(long_greeting)
        else:
            salutation_patterns.append([{"LOWER": greeting},{"TEXT":{"REGEX": "([^\s]+)"}}])  
            
            
    label = "pat_salutation"
    salutation_recognizer = KeyWordRecognizer(
        nlp, patterns=salutation_patterns, label=label
    )
    add_recognizer(label=label, recognizer=salutation_recognizer)



    ###############################################################
    # clincian salutation
    ###############################################################
    clinician_pattern = [{"LOWER": "hello"}, {"LOWER": "dr"},{'IS_PUNCT':True}, {"ENT_TYPE": "PERSON"}]
    clinician_pattern2 = [{"LOWER": "hi"}, {"LOWER": "dr"},{'IS_PUNCT':True}, {"ENT_TYPE": "PERSON"}]
    clinician_pattern3 = [{"LOWER": "dear"}, {"LOWER": "dr"},{'IS_PUNCT':True}, {"ENT_TYPE": "PERSON"}]
    clinician_patterns = [clinician_pattern, clinician_pattern2, clinician_pattern3]

    label = "clinician_salutation"
    clinician_recognizer = KeyWordRecognizer(
        nlp, patterns=clinician_patterns, label=label
    )
    add_recognizer(label=label, recognizer=clinician_recognizer)

    proxy_pattern1 = [{"LOWER": "sent"},{"LOWER": "by"},
                    {"TEXT":{"REGEX": "([^\s]+)"}},{"TEXT":{"REGEX": "([^\s]+)"}},{"LOWER": "on"},{"LOWER": "behalf"},{"LOWER": "of"},
                    {"TEXT":{"REGEX": "([^\s]+)"}},{"TEXT":{"REGEX": "([^\s]+)"}}] # 


    label = "proxy_declaration"

    proxy_patterns = [proxy_pattern1]
    proxy_recognizer = KeyWordRecognizer(nlp, patterns=proxy_patterns, label=label)
    add_recognizer(label=label, recognizer=proxy_recognizer)

    ###############################################################
    # signature
    ###############################################################
    signature_pattern = [{"LOWER": "sincerely"},{"IS_PUNCT": True},{"ENT_TYPE": "PERSON"},]
    signature_pattern2 = [{"LOWER": "thanks"},{"IS_PUNCT": True},{"ENT_TYPE": "PERSON"},]
    signature_pattern3 = [{"LOWER": "thank"},{"LOWER": "you"}, {"IS_PUNCT": True},{"ENT_TYPE": "PERSON"},]
    signature_pattern4 = [{"LOWER": "thanks"},{"LOWER": "so"}, {"LOWER": "much"},{"IS_PUNCT": True},{"ENT_TYPE": "PERSON"},]
    signature_pattern5 = [{"LOWER": "best"}, {"IS_PUNCT": True}, {"ENT_TYPE": "PERSON"}]
    signature_pattern6 = [{"LOWER": "thank"},{"LOWER": "you"}, {"IS_SPACE": True},{"ENT_TYPE": "PERSON"},]
    signature_pattern7 = [{"LOWER": "thanks"},{"IS_SPACE": True},{"ENT_TYPE": "PERSON"},]
    signature_pattern8 = [{"LOWER": "thanks"},{"IS_SPACE": True},{"IS_PUNCT": True},{"ENT_TYPE": "PERSON"},]
    signature_pattern9 = [{"LOWER": "sincerely"},{"IS_SPACE": True},{"IS_PUNCT": True},{"ENT_TYPE": "PERSON"},]

    label = "signature"
    signature_patterns = [signature_pattern,signature_pattern2,signature_pattern3,signature_pattern4,signature_pattern5,signature_pattern6,signature_pattern7,signature_pattern8,signature_pattern9]
    signature_recognizer = KeyWordRecognizer(
        nlp, patterns=signature_patterns, label=label
    )
    add_recognizer(label=label, recognizer=signature_recognizer)

    ###############################################################
    # salutation (multiple)
    ###############################################################
    ruler = EntityRuler(nlp,overwrite_ents=True)
    patterns = [
                # salutation
                {"label": "pat_salutation_multi", "pattern": [{'ENT_TYPE':'pat_salutation'},{'LOWER':'and'},{"TEXT":{"REGEX": "([^\s]+)"}} ]},
                # signature
                {"label": "signature_multi", "pattern": [{'ENT_TYPE':'signature'},{'LOWER':'and'},{"TEXT":{"REGEX": "([^\s]+)"}} ]} 
            ]
    ruler.add_patterns(patterns)
    nlp.add_pipe(ruler)
    
    ###############################################################
    # co-reference resolution using huggingface package
    ###############################################################
    neuralcoref.add_to_pipe(nlp)


    print('Custom NLP pipeline loaded: {}'.format([n[0] for n in nlp.pipeline]))
    return nlp