#!/usr/bin/python

__author__ = "Austin Powell"
__license__ = "MIT"
__version__ = "0.5"
__maintainer__ = "Austin Powell"
__email__ = "aupowell@stanfordchildrens.org"
__status__ = "Research"

## Standard Libraries
import numpy as np

"""Module Description:
Functions to provide occasional information on a teens dataset that is frequently referenced
"""



def teens_cohort_describe(dataframe):
    """Prints out standardized information about the MyChart Teens Dataframe"""
    print('----------------- Dataset Cohort Information ----------------------')
    print('Dataframe shape: {:,}'.format(dataframe.shape[0]))
    print('Num patients: {:,}'.format(len(dataframe.pat_id_patient_myc.unique())))
    print('Num proxy accounts: {:,}'.format(len(dataframe[dataframe['has_proxy_heuristic'] == 1].pat_id.unique())))
    print('Num messages: {:,}'.format(len(dataframe.myc_message_id.unique())))
    print('Cohort median num messages per patient: {}'.format(np.median(dataframe.groupby("pat_id")["pat_id_patient_myc"].count())))

