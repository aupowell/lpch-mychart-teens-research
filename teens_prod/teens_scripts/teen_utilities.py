#!/usr/bin/python

__author__ = "Austin Powell"
__license__ = "MIT"
__version__ = "0.5"
__maintainer__ = "Austin Powell"
__email__ = "aupowell@stanfordchildrens.org"
__status__ = "Research"

## Utility
import numpy as np
import pandas as pd
import os
import gc
import pickle
import re
import spacy
from sklearn.model_selection import train_test_split
import swifter
import datetime


################################################################################
# Globals
################################################################################
# alphabetic characters only
alpha_regex = re.compile('[^a-zA-Z]')


################################################################################
# Create columns to compare patient name to mychart email on record
#################################################################################
def return_email_name(txt):
    """gets name of email address without domain.
    Args:
        Input:
            txt: cell string from pandas
        Output:
            txt: standardized name
    """
    txt = str(txt)
    if '@' in txt:
        # get txt before @
        txt = txt.split('@')[0]
        # standardize name for email address
        txt = txt.strip().lower()
        # remove all non-alphabetic characters to standardize
        txt = alpha_regex.sub('', txt)
        
        return txt
    else:
        return txt

def return_email_domain(txt):
    """Split email addresses ignoring missing spaces.
    Args:
        Input:
            txt: email address as string
        Output:
            txt: email domain name. E.g. "gmail.com"
    """
    try:
        return str(txt).split('@')[1].strip().lower()
    except:
        return txt

def arrange_name_firstlast(txt):
    """ Compact and arranges name to be FIRSTLAST instead of last, first.
    Args:
        Input:
            txt: string of surname and first name seperated by comma.
        Output:
            txt: string in the form of firstlast without the comma.
    """
    txt = str(txt)
    if ',' in txt:
        l,f = txt.split(',')
        full_name = f.strip().lower()+l.strip().lower()
        full_name = ''.join(full_name.split(' '))
        return full_name
    else:
        return txt

def arrange_name_lastfirst(txt):
    """ Compact and arranges name to be LASTFIRST instead of last, first.
    Args:
        Input:
            txt: string of surname and first name seperated by comma.
        Output:
            txt: string in the form of firstlast without the comma.
    """
    txt = str(txt)
    if ',' in txt:
        l,f = txt.split(',')
        full_name = l.strip().lower()+f.strip().lower()
        full_name = ''.join(full_name.split(' '))
        return full_name
    else:
        return txt

def get_first_name(txt):
    """ Gets the first name (ignoring multiple names in firstname place)
    Args:
        Input:
            txt: string of surname and first name seperated by comma.
        Output:
            txt: first name. Cannot have whitespace.
    """
    txt = str(txt)
    if "," in txt:
        last, first = txt.split(",")
        first_first = first.split()[0]

        return first_first.lower()
    else:
        return txt.lower()

def get_fullname(name):
    """Returns the name of patient in First Last form with space in between
    
    Args:
        Input: 
            name: string of patient name in the format "Smith, John"
    """
    try:
        last, first = name.split(',')
        return first.lower() + ' ' + last.lower()
    except:
        return ''
#####################################################################################

####################################################################################
## Group the messages together
####################################################################################
def concat_msg_txt(data):
    """Concatinates all text for a given message ID. Used when grouping by pat_id and message_id.
    Args:
        Input:
            data: pandas row
        Output:
            msg: concatinated messages (as defined by a groupby) seperated by whitespace.
    """
    msg = data['msg_txt'].str.cat(sep=' ')
    return msg

####################################################################################
## Mostly ETL
####################################################################################

def proxy_filter(doc):
    """uses proxy delcaration in text message as filter."""
    proxy_ents = [e for e in doc.ents if e.label_ == "proxy_declaration"]
    if len(proxy_ents) > 0:
        return 1
    else:
        return 0


def teen_in_salutation(doc):
    """Tries to see if the teen is in salutation.
    """
    pat_ents = [e for e in doc.ents if e.label_ == "pat_salutation"]
    if len(pat_ents) > 0:
        return pat_ents[0].text
    else:
        return None

def drop_duplicate_columns(dataframe=None):
    """Drops duplicate columns with warning
    """

    dataframe_copy = dataframe.copy()

    # dropping duplicate columns
    cs = list(dataframe_copy.columns)
    track_dropped = []
    for i,item in enumerate(dataframe_copy.columns):
        if item in dataframe_copy.columns[:i]:
            track_dropped.append(cs[i])
            cs[i] = "toDROP"

    print('Dropped duplicate columns: {}'.format(track_dropped))
    dataframe_copy.columns = cs
    dataframe_copy.drop("toDROP",1,inplace=True)

    return dataframe_copy

def tidy_dataframe(dataframe, pat_age_date = None):
    """Performs a few actions on dataframe imported from Clarity for  MyChart that make it easier to work with.

    Args:
        Input:
            dataframe: Dataframe (Pandas)
            pat_age_date: Date the patient's age should be reported as of (datetime)
        Output:
            dataframe_copy: Dataframe (Pandas)
    """

    dataframe_copy = dataframe.copy()
    print('Data import shape: {}'.format(dataframe_copy.shape))

    # lowercase all columns
    dataframe_copy.columns = [c.lower().strip() for c in dataframe_copy]
    # remove duplicate columns
    dataframe_copy = drop_duplicate_columns(dataframe=dataframe_copy)

    # get current age (as of running this pipeline)
    dataframe_copy.loc[:,'birth_date'] = pd.to_datetime(dataframe_copy['birth_date'])

    dataframe_copy.loc[:,'age'] = dataframe_copy['birth_date'].swifter.apply(lambda cell: (pat_age_date - cell).days / 365.26)

    # Output for notebook
    print('Tidied dataframe complete. Data Export shape: ')
    print()
    print('Shape: {}; Num messages: {}; num patients: {}'.format(dataframe_copy.shape,len(dataframe_copy.myc_message_id.unique()),len(dataframe_copy.pat_id_patient_myc.unique())))
    return dataframe_copy




def data_split(data=None, labels=None, train_per=0.8):
    """
    Inputs original dataset and returns a train/test/val split seeded.
    Inputs:
        data: (data excluding the label). Assumed to pandas or array.
        label: the array for the output
        train_per: set to 80/10/10
    Outputs:
        3 tuples of feature, label sets for train, val, test respectively
    """
    X_train, X_test, y_train, y_test = train_test_split(
        data, labels, test_size=0.4, random_state=416
    )
    X_val, X_test, y_val, y_test = train_test_split(
        X_test, y_test, test_size=0.5, random_state=416
    )

    return (X_train, y_train), (X_val, y_val), (X_test, y_test)


def create_data(dataframe=None):
    """
    Receives a pandas dataframe, creates features and returns X, y.
    Input: "dataframe": Pandas dataframe
    Output: "X, y": dataframe and label respectively
    """

    if dataframe is None:
        print("Dataframe is none")
        return None

    dataframe.drop_duplicates(subset=["msg_txt"], keep="first", inplace=True)

    dataframe["msg_txt"] = dataframe["msg_txt"].swifter.apply(
        lambda cell: str(cell).strip()
    )
    dataframe["first_subject"] = dataframe["first_subject"].swifter.apply(
        lambda cell: str(cell).strip()
    )

    X = dataframe.drop(["wrong_email"], 1)
    y = dataframe["wrong_email"]

    print("Dataset created. Shape: {}".format(X.shape))
    return X, y

def evaluate_label_dist(lbl_array):
    """Prints out proportions for labels:"""
    return "Percent positive labels: {:.3f}".format(np.mean(lbl_array))


def spacy_test():
    try:
        nlp('nlp test')
    except NameError:
        print('Spacy nlp object not found. Creating spacy object.')
        nlp = spacy.load('en_core_web_lg')
    else:
        print("Couldn't create spacy object")
    return nlp

def get_first(cell):
    """Applied to column containing pat_name. e.g. last, first returns first name
    """
    txt = str(cell)
    first = txt.split(",")[-1]

    if len(first.split()) > 1:
        first = first.split()[0]
        return first.lower()
    else:
        return first.lower()
def create_pat_name_features(dataframe=None):
    """ Creates various name features including creating a spacy doc column
    """


    def get_names(spacy_doc):
        """Extracts entities of person type in order to get names. Assumes a spacy object
        """
        names = [e.lemma_.lower() for e in spacy_doc.ents if e.label_ in ["PERSON"]]
        return names





    def first_in_msg(cell_msg_names, cell_pat_first):
        """ Applied to column containing list of names found in text and column containing first name of patient.
        If patient first name is found in list returns 1.
        """
        for n in cell_msg_names:
            if cell_pat_first in n:
                return 1
            else:
                return 0
        return 0

    # # test for spacy in #!/usr/bin/env python
    # nlp = spacy_test()
    #
    # # create spacy object to perform nlp on for different purposes
    # dataframe.loc[:, "msg_txt_spacy_doc"] = dataframe["msg_txt"].swifter.apply(lambda c: nlp(c))

    # get first name from pat_id
    dataframe.loc[:, "pat_first"] = dataframe["pat_name"].apply(
        lambda c: get_first(c)
    )

    dataframe.loc[:, "msg_txt_names"] = dataframe.apply(
        lambda cell: get_names(cell["msg_txt_spacy_doc"]), 1
    )

    # create feature: number of names found
    dataframe.loc[:, "num_msg_names"] = dataframe.apply(
        lambda cell: len(cell["msg_txt_names"]), 1
    )

    # create future: determins if pat_id first name was found in msg_txt
    dataframe.loc[:, "pat_first_in_msg"] = dataframe.apply(
        lambda c: first_in_msg(c["msg_txt_names"], c["pat_first"]), 1
    )

    return dataframe
