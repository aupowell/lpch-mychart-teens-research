#!/usr/bin/python

__author__ = "Austin Powell"
__license__ = "MIT"
__version__ = "0.5"
__maintainer__ = "Austin Powell"
__email__ = "aupowell@stanfordchildrens.org"
__status__ = "Research"

################################################################################
# Golden standard list for messages that contain a string and qualify them
# as being an automated notification of some sort
################################################################################
misleading_messages = [
    "e-mail changed",
    "appointment information",
    "you have new test results",
    "patient information has been updated automatically",
    "have a new test result",
    "logon to MyChart.StanfordChildrens.org".lower(),
    "don't wait until summer",
    "Your payment for account".lower(),
    "Información sobre la cita Información".lower(),
    "Our records indicate that it is time for".lower(),
    "It's that time of the year! Get protected from the flu!".lower(),
    "Appointment Request From".lower(),
    "Thank you for choosing Lucile Packard Children's Hospital".lower(),
    "This appointment has been cancelled as requested.".lower(),
    "will be 18 years old and in the state of California will be considered".lower(),
    "The flu vaccine is here".lower(),
    "Sincerely,  Patient Services".lower(),
    "Just a quick message to remind you of our Holiday Hours".lower(),
    "Based on our records it appears that it is time".lower(),
    "We are excited to announce that the Stanford Outpatient Center laboratory draw station in Redwood City".lower(),
    "If this is an emergency, please call 9-1-1 and go directly to your nearest emergency room for immediate care.".lower(),
    "Mobile phone changed for".lower(),
    "We are happy to announce that you can now schedule your upcoming appointments with".lower(),
    "We know that you have options when selecting your healthcare provider and we thank you for choosing Stanford Children's Health".lower(),
    "Please see the link below for more event details".lower(),
    "We are excited to announce".lower(),
    "Please contact your provider".lower(),
    "Clinic will be closed".lower(),
    "Appointment For:".lower(),
    "Dear Patient and/or Patient Family".lower(),
    "dear parent or guardian".lower(),
    "Dear Patients and Families:".lower(),
    "Thanks for signing up for MyChart.".lower(),
    "Our practice offering extended evening and Saturday hours for our patients.".lower(),
    "Your virtual visit is scheduled-please ensure all registration forms are submitted with the clinic".lower(),
    "Demographics have been updated automatically via MyChart. This message is for information only.".lower(),
    "Hi there! Another MyChart message with great resources!".lower(),
    "Dear Stanford Children's Health patient".lower(),
    "We look forward to your upcoming Clinic to Home Telehealth Virtual visit.".lower(),
    "Información de Cita Información".lower(),
    "When paying for parking the vehicle parked will be identified by the".lower(),
    "This is an informative message to inform you that our".lower(),
    "Appointment canceled for ".lower(),
    "school year quickly approaching, and we would like to send you a quick reminder that we would be happy to help you with camp and school".lower(),
    "Thank you for contacting Stanford Children’s Health. We appreciate your participation in our Telehealth Pilot.".lower(),
    "Your credit card was not charged for the following appointment.".lower(),
    "new version has been released and is required for all future telehealth virtual visits.".lower(),
    "would like to cancel the following appointments:".lower(),
    "Questionnaire: Stanford Children's Telehealth Virtual Visit".lower(),
    "Release records to:  , AnotherPerson ,  ,Recipient Information:  ,To Person/Facility:".lower()

]


def notification_filter(dataframe=None):
    """Filters out notifications by eliminating key phrases. Returns new dataframe
    dataframe: pandas dataframe that contains a column msg_txt.
    """
    msgs = dataframe.msg_txt.tolist()
    notification_idxs = [
        m
        for m in range(len(msgs))
        if any(e in str(msgs[m]).lower() for e in misleading_messages)
    ]

    dataframe = dataframe[~dataframe.index.isin(notification_idxs)]
    return dataframe
    
def notification_labeler(dataframe = None):
    """Create a column labeled "notification_label", denoting whether the given concatinated message is a notification
    based off of f the notification list above.
    """
    def is_notification(row):
        # string-based labeler
        if any(e in str(row['msg_txt']).lower() for e in misleading_messages):
            return 1
        # column-based labeler
        elif row["myc_msg_typ_c"] == 999: # 999 - System Message (based on Epic Data Dictionary)
            return 1
        else:
            return 0

    dataframe.loc[:,'notification_label'] = dataframe.apply(lambda row: is_notification(row = row),1)
    

    return dataframe
