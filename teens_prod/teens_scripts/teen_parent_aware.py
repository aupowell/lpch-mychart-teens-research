#!/usr/bin/python

__author__ = "Austin Powell"
__license__ = "MIT"
__version__ = "0.5"
__maintainer__ = "Austin Powell"
__email__ = "aupowell@stanfordchildrens.org"
__status__ = "Research"

import nltk
from nltk import word_tokenize, pos_tag, ne_chunk
from spacy.tokenizer import Tokenizer
from spacy.lang.en import English
import textdistance
from pyjarowinkler import distance

# Custom Modules
import importlib
importlib.import_module('teens_prod')
import teens_prod



#############################
# Spacy initiliazation for tokenizer
#############################
nlp_tok = English()
# Create a blank Tokenizer with just the English vocab
tokenizer = nlp_tok.Defaults.create_tokenizer(nlp_tok)
#########################################################################################
# Functions
#########################################################################################
def get_first_name(txt):
    """Gets the first name of a full string name of form "last, first". Pandas function.
    
    Args:
        Input:
            txt: string
        Output:
            txt: string
    """
    txt = str(txt)
    if "," in txt:
        last, first = txt.split(",")
        first_first = first.split()[0]

        return first_first.lower()
    else:
        return txt.lower()

def fn_preprocess(art):
    """Used in "possessive_teen" function to get parts of speech. Pandas apply function.

    Args:
        Input:
            art: string corpus from message for teen_3rd_person
        Output:
            art: list of tokens
    """
    art = [t.text for t in tokenizer(art)] # Spacy default tokenizer
    art = nltk.pos_tag(art)
    return art

def possessive_teen(cell):
    """Gets part of speech word tags.

    Args:
        Input:
            cell: pandas cell
        Output:
            pat_3rd_person_reference: binary for whether patient was referenced in 3rd person or not.
    """
    nltk_toks = fn_preprocess(str(cell["msg_txt"]))

    pat_3rd_person_reference = 0
    for idx in range(len(nltk_toks) - 1):
        if cell["pat_name_first"] in nltk_toks[idx][0].lower():

            if (nltk_toks[idx + 1][1] == "POS") or (cell["pat_name_first"] == nltk_toks[idx][0].lower()+'s'):

                pat_3rd_person_reference = 1
                break
            else:
                pass
        else:
            pass

    return pat_3rd_person_reference


def hi_not_teen(cell):
    """Checks if parent first name is in salutation. Returns true if occurs at least once; 0 otherwise. Pandas apply function.
    Assumes that Spacy pipeline has already been applied.

    Args:
        Input:
            cell: Pandas cell for apply
        Output:
    """
    doc = cell["msg_txt_spacy_doc"]
    sals = [e.text.lower() for e in doc.ents if e.label_ == "pat_salutation"]
    num_hi_parent = [hi for hi in sals if str(cell["pat_rel_name_first"]).lower() in hi.lower()]
    if len(num_hi_parent) > 0:
        return 1
    else:
        return 0

def relable_dead_giveaways(cell):
    """Parent reference to their child in the message.

    Args:
        Input:
            cell: Pandas cell for apply.
        Output:
            binary output for whether some giveaway strings were found in the text or not.
    """
    dead_giveaways = [
        "my daughter",
        "my son",
        "we left the appointment",
        "letting us",
        "your son",
        "your daughter",
        "your child",
    ]
    if any(ext in str(cell["msg_txt"]).lower() for ext in dead_giveaways):
        return 1
    else:
        return 0

def relable_multi_salutation(cell):
    """Relables if there is a greeting to more than one person. Pandas data cell.
    Assumes that Spacy pipeline has already been applied.

    Args:
        Input:
            cell: pandas cell for apply method.
        Output:
            binary out put for whether an entity was found to contain pat in salutation.
    """
    doc = cell["msg_txt_spacy_doc"]
    sals = [e.text.lower() for e in doc.ents if e.label_ == "pat_salutation_multi"]
    if len(sals) > 0:
        return 1
    else:
        return 0


def teen_3rd_person(cell):
    """Teen referenced (not in possessive form) in 3rd person in salutation.
    Assumes that Spacy pipeline has already been applied.

    Args:
        Input:
            cell: pandas cell for apply method
        Output:
            binary output for whether named entities applied to text where found
    """
    doc = cell["msg_txt_spacy_doc"]
    if cell['possessive_teen_name'] != 1:
        sals = [e.text.lower() for e in doc.ents if e.label_ in ["pat_salutation_multi","pat_salutation"]]
        num_teen_in_hi = [hi for hi in sals if str(cell["pat_name_first"]).lower() in hi.lower()]
        if len(num_teen_in_hi) == 0:
            if str(cell["pat_name_first"]).lower() in str(cell['msg_txt']).lower():
                return 1
            else:
                return 0
        else:
            return 0
    else:
        return 0


def num_teen_3rd_referenced(row):
    """Uses nlp doc features (a huggingface library) to say whether a teen's name has an entity resolution reference to it.
    
    Args:
        Input:
            row: a row from a pandas dataframe

        Output:
            teen_ref_count: The number of times a reference to the teen was made as a resolved entity.
    """
    
    doc = row['msg_txt_spacy_doc']

    # Gets first name of the teen
    first_name = teens_prod.teens_scripts.teen_utilities.get_first_name(row['pat_name'])
    teen_ref_count = 0
    
    # checks that there were any coreferrences
    if doc._.has_coref:
        # Iterate through references
        for ent in doc._.coref_clusters:
            ent_ref = ent[0].lower_
            if first_name in ent_ref:
                for ref in ent:
                    # This is a rough string match and potentially a failing point
                    # It is just a string match
                    if first_name not in ref.lower_:
                        teen_ref_count += 1
    return teen_ref_count

def num_teen_referenced_binary(row=None):
    """Binary if a teen's name has any number >0 of co-entities in the corpus.
    Corresponding columns is 'num_teen_references' which is the non-binary version of this one.
    
    Args:
        Input:
            row: pandas 
        Output: 1 or 0 
        
    """
    if row['num_teen_referenced'] > 0:
        return 1
    else:
        return 0


# def identify_parent(row=None, column=None):
#     """Returns closest match based on character matching to email name from list of parent's names.
#     Args:
#         Input:
#             column: the column containing a list of concatinated parent's names
#         Output:
#             min distance for a parent's name
#     """
#     parent_names = row[column]

#     # store distances for all names
#     dists = []

#     # check for no parents
#     if parent_names[0] != None:

#         # Iterate through parent names (lastfirst for now)
#         for name in parent_names:
#             dist = textdistance.levenshtein(name, row["pat_email_name_standardized"])
#             #dist = textdistance.jaro_winkler(name, row["pat_email_name_standardized"])
#             dists.append(dist)
#         return min(dists)
#     else:
#         return None


# def min_dist_email(row=None):
#     """ Gets the minimum distance when ordering from firstlast or last first
#     """
#     return min(
#         row["min_dist_parent_name_lastfirst"], row["min_dist_parent_name_firstlast"]
#     )

# def apply_email_cutoff(row=None,cutoff=None):
#     """Applies classification of email address being parent or teens based off of a string
#     similarity metric and custom logic"""

#     if row['min_name_dist'] <= cutoff:
#         # if patient's full first name in email address
#         if row['pat_name_first'] in row['pat_email_name_standardized']:
#             return 0
#         else:
#             return 1
#     else:
#         return 0



def concat_names_parent_rel_firstlast(cell=None):
    """Concats parents names from list. Form of firstlast. Pandas apply function.
    Args:
        Input:
            cell: pandas cell
        Output:
            standardized_names: list of strings. firstlast
    """
    names = cell['pat_rel_list']
    assert type(names) == list
    standardized_names = []
    for name in names:
        standardized_names.append(teens_prod.teens_scripts.teen_utilities.arrange_name_firstlast(name))
    return standardized_names

def concat_names_parent_rel_lastfirst(cell=None):
    """Concats parents names from list. Form of lastfirst. Pandas apply function.
    Args:
        Input:
            cell: pandas cell
        Output:
            standardized_names: list of strings. firstlast
    """
    names = cell['pat_rel_list']
    assert type(names) == list
    standardized_names = []
    for name in names:
        standardized_names.append(teens_prod.teens_scripts.teen_utilities.arrange_name_lastfirst(name))
    return standardized_names


def contains_on_behalf(row):
    """Simple string match to see if key text indicating a message was sent from a proxy account."""
    if "on behalf of" in str(row['msg_txt']).lower():
        return 1
    else:
        return 0