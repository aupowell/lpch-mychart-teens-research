#!/usr/bin/python

__author__ = "Austin Powell"
__license__ = "MIT"
__version__ = "0.5"
__maintainer__ = "Austin Powell"
__email__ = "aupowell@stanfordchildrens.org"
__status__ = "Research"


"""Module Description:
Functions in order to facilitate identifying parent accounts by similarity of email address to a list of relative names
"""

## Standard Libraries
import numpy as np
import re

## 3rd party libaries
import textdistance

################################################################################
# Globals
################################################################################
# alphabetic characters only
alpha_regex = re.compile('[^a-zA-Z]')

################################################################################
# Create columns to compare patient name to mychart email on record
#################################################################################
def return_email_name(txt):
    """gets name of email address without domain.
    Args:
        Input:
            txt: cell string from pandas
        Output:
            txt: standardized name
    """
    txt = str(txt)
    if '@' in txt:
        # get txt before @
        txt = txt.split('@')[0]
        # standardize name for email address
        txt = txt.strip().lower()
        # remove all non-alphabetic characters to standardize
        txt = alpha_regex.sub('', txt)
        
        return txt
    else:
        return txt


def min_dist_email(row=None):
    """ Gets the minimum distance when ordering from firstlast or last first
    """
    return min(
        row["min_dist_parent_name_lastfirst"], row["min_dist_parent_name_firstlast"]
    )

def apply_email_cutoff(row=None,cutoff=None):
    """Applies classification of email address being parent or teens based off of a string
    similarity metric and custom logic"""

    if row['min_name_dist'] <= cutoff:
        # if patient's full first name in email address
        if row['pat_name_first'] in row['pat_email_name_standardized']:
            return 0
        else:
            return 1
    else:
        return 0

def identify_parent(row=None, column=None):
    """Returns closest match based on character matching to email name from list of parent's names.
    Args:
        Input:
            column: the column containing a list of concatinated parent's names
        Output:
            min distance for a parent's name
    """
    parent_names = row[column]

    # store distances for all names
    dists = []

    # check for no parents
    if parent_names[0] != None:

        # Iterate through parent names (lastfirst for now)
        for name in parent_names:
            dist = textdistance.levenshtein(name, row["pat_email_name_standardized"])
            #dist = textdistance.jaro_winkler(name, row["pat_email_name_standardized"])
            dists.append(dist)
        return min(dists)
    else:
        return None