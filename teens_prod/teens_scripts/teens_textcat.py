
#!/usr/bin/env python
# coding: utf8
"""Train a convolutional neural network text classifier on the
IMDB dataset, using the TextCategorizer component. The dataset will be loaded
automatically via Thinc's built-in dataset loader. The model is added to
spacy.pipeline, and predictions are available via `doc.cats`. For more details,
see the documentation:
* Training: https://spacy.io/usage/training
Compatible with: spaCy v2.0.0+
"""
from __future__ import unicode_literals, print_function
import plac
import random
from pathlib import Path
import thinc.extra.datasets

import spacy
import pandas as pd
import numpy as np
from sklearn.metrics import roc_auc_score
import os
import swifter
from spacy.util import minibatch, compounding
from sklearn import metrics

from teen_utilities import data_split, create_data, evaluate_label_dist

@plac.annotations(
    model=("Model name. Defaults to blank 'en' model.", "option", "m", str),
    output_dir=("Optional output directory", "option", "o", Path),
    n_texts=("Number of texts to train from", "option", "t", int),
    n_iter=("Number of training iterations", "option", "n", int),
    learn_rate=("Learning rate", "option", "lr", float),
    init_tok2vec=("Pretrained tok2vec weights", "option", "t2v", Path),
)
def main(model=None, output_dir=None, n_iter=20, n_texts=2000, learn_rate = 2e-5,init_tok2vec=None):
    if output_dir is not None:
        output_dir = Path(output_dir)
        if not output_dir.exists():
            output_dir.mkdir()

    if model is not None:
        nlp = spacy.load(model)  # load existing spaCy model
        print("Loaded model '%s'" % model)
    else:
        nlp = spacy.blank("en")  # create blank Language class
        print("Created blank 'en' model")

    # add the text classifier to the pipeline if it doesn't exist
    # nlp.create_pipe works for built-ins that are registered with spaCy
    if "textcat" not in nlp.pipe_names:
        textcat = nlp.create_pipe(
            "textcat", config={"exclusive_classes": True, "architecture": "simple_cnn"}
        )
        nlp.add_pipe(textcat, last=True)
    # otherwise, get it, so we can add labels to it
    else:
        textcat = nlp.get_pipe("textcat")

    # add label to text classifier
    textcat.add_label("POSITIVE")
    textcat.add_label("NEGATIVE")

    # load the IMDB dataset
    print("Loading IMDB data...")
    (train_texts, train_cats), (dev_texts, dev_cats) = load_data()
    train_texts = train_texts[:n_texts]
    train_cats = train_cats[:n_texts]
    print(
        "Using {} examples ({} training, {} evaluation)".format(
            n_texts, len(train_texts), len(dev_texts)
        )
    )
    train_data = list(zip(train_texts, [{"cats": cats} for cats in train_cats]))

    # get names of other pipes to disable them during training
    other_pipes = [pipe for pipe in nlp.pipe_names if pipe != "textcat"]
    with nlp.disable_pipes(*other_pipes):  # only train textcat
        optimizer = nlp.begin_training(lr=learn_rate)
        if init_tok2vec is not None:
            with init_tok2vec.open("rb") as file_:
                textcat.model.tok2vec.from_bytes(file_.read())
        print("Training the model...")
        print("{:^5}\t{:^5}\t{:^5}\t{:^5}\t{:^5}".format("Train LOSS", "Val P", "Val R", "Val F1","Val AUC"))
        batch_sizes = compounding(2.0, 16.0, 1.001)
        for i in range(n_iter):
            losses = {}
            # batch up the examples using spaCy's minibatch
            random.shuffle(train_data)
            batches = minibatch(train_data, size=batch_sizes)
            for batch in batches:
                texts, annotations = zip(*batch)
                nlp.update(texts, annotations, sgd=optimizer, drop=0.4, losses=losses)
            with textcat.model.use_params(optimizer.averages):
                # evaluate on the dev data split off in load_data()
                scores = evaluate(nlp.tokenizer, textcat, dev_texts, dev_cats)
            print(
                "{0:.3f}\t{1:.3f}\t{2:.3f}\t{3:.3f}\t{4:.3f}".format(  # print a simple table
                    losses["textcat"],
                    scores["textcat_p"],
                    scores["textcat_r"],
                    scores["textcat_f"],
                    scores["auc"]
                )
            )

    # test the trained model
    test_text = "Forgot to mention that Nate's blood count has been dropping. I know they have both been anemic in the past, but we always have to look out for blood loss through the gut. If I put in stool blood testing order via quest or labcorp, would you be able to pick up the testing cards and have nathan test his stools over 3 days?"
    doc = nlp(test_text)
    print(test_text, doc.cats)

    if output_dir is not None:
        with nlp.use_params(optimizer.averages):
            nlp.to_disk(output_dir)
        print("Saved model to", output_dir)

        # test the saved model
        print("Loading from", output_dir)
        nlp2 = spacy.load(output_dir)
        doc2 = nlp2(test_text)
        print(test_text, doc2.cats)



def teens_load_data():
    ## Load data
    ## Data folder
    data_folder = "./../data/ml_data"
    model_folder = "./../models/"

    #### Filenames
    # fname = 'mychart_full_initial.csv'
    # fname = "df_email_labeled_msg.csv"
    fname = "df_labeled_msg_unique.csv"


    ## Data pipeline
    # Load data
    dataframe = pd.read_csv(os.path.join(data_folder, fname))
    # create data
    X, y = create_data(dataframe=dataframe)
    return X, y

def load_data(limit=0, split=0.8):
    """Load data from the IMDB dataset."""


    ####### SPACY DEFAULT ###########################
    # Partition off part of the train data for evaluation
    # train_data, _ = thinc.extra.datasets.imdb()
    # random.shuffle(train_data)
    # train_data = train_data[-limit:]
    # texts, labels = zip(*train_data)

    # cats = [{"POSITIVE": bool(y), "NEGATIVE": not bool(y)} for y in labels]
    # split = int(len(train_data) * split)
    # return (texts[:split], cats[:split]), (texts[split:], cats[split:])

    ################# TEENS #############################
    X, y = teens_load_data()
    # Split data
    train, val, test = data_split(data=X, labels=y)
    print("Shapes")


    X_train, y_train = train[0], train[1]
    X_val, y_val = val[0], val[1]
    X_test, y_test = test[0], test[1]

    X_train = pd.concat([X_train,X_test],1)
    print(X_train.msg_txt)
    y_train = y_train.tolist() + y_test.tolist()
    
    train_cats = [{"POSITIVE": bool(y), "NEGATIVE": not bool(y)} for y in y_train]
    dev_cats = [{"POSITIVE": bool(y), "NEGATIVE": not bool(y)} for y in y_val]
    return (X_train.msg_txt.tolist(), train_cats), (X_val.msg_txt.tolist(), dev_cats)



def evaluate(tokenizer, textcat, texts, cats):
    docs = (tokenizer(text) for text in texts)
    tp = 0.0  # True positives
    fp = 1e-8  # False positives
    fn = 1e-8  # False negatives
    tn = 0.0  # True negatives

    scores = []
    labels = []
    for i, doc in enumerate(textcat.pipe(docs)):
        gold = cats[i]
        for label, score in doc.cats.items():
            labels.append(label)
            scores.append(score)
            if label not in gold:
                continue
            if label == "NEGATIVE":
                continue
            if score >= 0.5 and gold[label] >= 0.5:
                tp += 1.0
            elif score >= 0.5 and gold[label] < 0.5:
                fp += 1.0
            elif score < 0.5 and gold[label] < 0.5:
                tn += 1
            elif score < 0.5 and gold[label] >= 0.5:
                fn += 1
    precision = tp / (tp + fp)
    recall = tp / (tp + fn)
    if (precision + recall) == 0:
        f_score = 0.0
    else:
        f_score = 2 * (precision * recall) / (precision + recall)

    binary_labels = []
    for l in labels:
        if l == "POSITIVE":
            binary_labels.append(1)
        else:
            binary_labels.append(0)

    auc = roc_auc_score(binary_labels, scores)



    return {"textcat_p": precision, "textcat_r": recall, "textcat_f": f_score, 'auc':auc}


if __name__ == "__main__":
    plac.call(main)