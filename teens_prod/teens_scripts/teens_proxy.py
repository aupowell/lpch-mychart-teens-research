#!/usr/bin/python

__author__ = "Austin Powell"
__license__ = "MIT"
__version__ = "0.5"
__maintainer__ = "Austin Powell"
__email__ = "aupowell@stanfordchildrens.org"
__status__ = "Research"


def has_proxy(row):
    """Heuristic based on different columns for determining if a patient has a proxy on file."""
    
    if (row['proxy_pat_id'] != None) | (row['proxypatidlist'] != None):
        return 1
    else:
        return 0