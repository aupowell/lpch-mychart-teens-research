#########################################################################
# Unit tests for:
# * Ensuring critical components of nlp pipeline are working properly
#########################################################################
import neuralcoref
import pytest

@pytest.fixture
def get_spacy():
    import spacy
    return spacy.load('en')

def test_load_neuralcoref(get_spacy):
    nlp = get_spacy
    neuralcoref.add_to_pipe(nlp)
    assert 'neuralcoref' in [n[0] for n in nlp.pipeline]

    doc = nlp(u'My sister has a dog. She loves him.')
    assert doc._.has_coref

    assert doc._.coref_resolved =='My sister has a dog. My sister loves a dog.'   ## 'My sister has a dog. My sister loves a dog.'

