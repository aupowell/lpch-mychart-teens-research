#########################################################################
# Unit tests for different data tasks:
# * Importing data
# * Cleaning data
# * Transforming data
#########################################################################
import pytest
import urllib.request
import ipaddress
import sys
import os

# Custom
sys.path.append('../')
#from teens_scripts import teen_ner
import teens_prod


def test_internet_connection(host='http://google.com'):
    """Tests for any internet connectivity.
    """
    try:
        urllib.request.urlopen(host) #Python 3.x
        return True
    except:
        return False

def test_local_data_exisits():
    """Tests if current working standard for local data exists.
    """
    files = os.listdir('./../../../data/')
    assert 'mychart_teens_full_022120.parquet' in files

