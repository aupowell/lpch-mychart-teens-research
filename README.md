# LPCH Mychart Teens Research
* **Code Contact:** Austin Powell
* **Code Contact Email:** aupowell@stanfordchildrens.org
* **Other Contacts:**
    * Wui Ip: WIp@stanfordchildrens.org
    * Keith Morse: KMorse@stanfordchildrens.org
    * James Xie: JXie@stanford.edu

## Purpose
Identification of parental activity in teen's mychart accounts and perform analysis across different institutions.

## Instructions
* **Installation**
    * 1) Create virtual environment: *python3 -m venv corefenv*
    * 2) Activate: source corefenv/bin/activate (ensure running python >=3.7.4: python -V)
    * 3) Install packages: pip3 install -r requirements.txt
    * 4) Add corefenv to jupyter to run notebooks: (corefenv)$ ipython kernel install --name "corefenv" --user
* **Recommend Process**
    * Run *mychart_teens_research_query.sql* to get string formated dataset
    * Run (if not already in some sort of performant flat columnar storage format) *utility_string_to_parquet* to get a more performat and efficient dataset.
    * Run *mychart_teens_research_0* to get cohort dataset
    * *mychart_teens_research_1* and *mychart_teens_research_2*

## File Structure
* **Notebooks**
    * **mychart_teens_research_0:** Initial data pipeline creating study cohort
    * **mychart_teens_research_1:** Standardized analysis
    * **mychart_teens_research_message_verification:** Manual verification of NLP algorithm
    * **mychart_teens_research_appendix:** Notebook included as a way to share knowledge with other institutions of some of the cohort questions LPCH was asking. Notebook is in a fairly raw form but we are happy to answer any clarifying questions.
    * **utility_string_to_parquet:** (optional) convert initial data pull made with sql to parquet format for larger data

* **teens_prod**
    * Python modules used in notebooks. Imported locally in notebooks as custom utilities.

* **mychart_teens_research_query.sql** Code to be run against Clarity instance
