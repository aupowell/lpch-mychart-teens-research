-- =============================================
-- Author:      Austin Powell
-- Contact: aupowell@stanfordchildrens.org
-- Create date: 05/29/20
-- Description: Query to retrieve data needed in MyChart Teens research collaborative effort. Resultant data to be used in Python notebook
-- =============================================


WITH
-- Concatinated
extern AS (
	SELECT DISTINCT
		MESSAGE_ID
	FROM
		dbo.MSG_TXT
),
-- PATIENT RACE NAMES
patient_race_latest AS (
	SELECT
		m1.pat_id AS pat_id_race,
		m1.PATIENT_RACE_C
	FROM
		dbo.PATIENT_RACE m1
	LEFT JOIN dbo.PATIENT_RACE m2 ON (m1.PAT_ID = m2.PAT_ID
			AND m1.LINE > m2.LINE)
WHERE
	m2.LINE IS NULL
),

-- PROXY PAT IDs
proxy_pat_list AS (
	SELECT DISTINCT
		s.PAT_ID,
		STUFF ((
				SELECT
					'|' + CAST(PROXY_PAT_ID AS NVARCHAR (MAX))
				FROM
					dbo.PAT_MYC_PRXY_ACSS s1
				WHERE
					s1.PAT_ID = s.PAT_ID
				ORDER BY
					line FOR XML PATH('')),
				1,
				1,
				'') AS ProxyPatIDList
		FROM
			dbo.PAT_MYC_PRXY_ACSS s
)

--PATIENT_MYC columns
SELECT
	TBL1.pat_id AS pat_id_patient_myc,
	TBL1.pat_access_code,
	TBL1.mychart_status_c,
	TBL1.RECV_EMAIL_YN,
	TBL1.DEACT_ACCT_YN,
	TBL2.*,
	TBL3.*,
	TBL4.*,
	TBL5.*,
	TBL6.*

	---------------------------------------------------------------------------------------------
	-- TBL1: Columns information about MyChart Patient Sign-up
	---------------------------------------------------------------------------------------------
FROM
	dbo.patient_myc TBL1
	---------------------------------------------------------------------------------------------
	-- TBL2: More Columns information about MyChart Patient Sign-up
	---------------------------------------------------------------------------------------------
	INNER JOIN (
		SELECT
			*
		FROM
			dbo.myc_patient) TBL2 ON TBL1.pat_id = TBL2.pat_id
	---------------------------------------------------------------------------------------------
	-- TBL3:Information from PATIENT Table
	---------------------------------------------------------------------------------------------
	INNER JOIN (
		SELECT
			pat_id AS patient_pat_id,
			pat_mrn_id AS child_pat_mrn_id,
			pat_name,
			birth_date,
			email_address AS child_email_address,
			sex_c AS child_sex,
			ethnic_group_c AS child_ethnic_group_c,
			religion_c AS child_religion_c,
			city AS child_city,
			state_c AS child_state_c,
			zip AS child_zip,
			LANGUAGE_C AS child_language_c,
			child_language_c_name,
			PATIENT_RACE_C,
			patient_race_name
		FROM
			dbo.patient pt1
			-- LANGUAGE NAMES
			LEFT OUTER JOIN (
				SELECT
					NAME AS child_language_c_name,
					LANGUAGE_C AS zc_language_c
				FROM
					dbo.ZC_LANGUAGE) pt2 ON pt1.LANGUAGE_C = pt2.zc_language_c
				-- PATIENT RACE
				LEFT OUTER JOIN (
					SELECT
						patient_race_latest.pat_id_race,
						patient_race_latest.PATIENT_RACE_C,
						ZC_PATIENT_RACE.NAME AS patient_race_name
					FROM
						patient_race_latest
						LEFT JOIN dbo.ZC_PATIENT_RACE ZC_PATIENT_RACE ON (patient_race_latest.PATIENT_RACE_C = ZC_PATIENT_RACE.PATIENT_RACE_C)) pt3 ON pt1.pat_id = pt3.pat_id_race) TBL3 ON TBL1.pat_id = TBL3.patient_pat_id
	---------------------------------------------------------------------------------------------
	-- TBL4: MYC Messages Information
	---------------------------------------------------------------------------------------------
	INNER JOIN (
		-- Concatinate messages based on message ID
		SELECT
			MYC_MESG.PAT_ID AS myc_pat_id,
			MYC_MESG.MESSAGE_ID AS myc_message_id,
			MYC_MESG.PARENT_MESSAGE_ID,
			MYC_MESG.CREATED_TIME,
			MYC_MESG.UPDATE_DATE,
			MYC_MESG.MYC_MSG_TYP_C,
			MYC_MESG.FROM_USER_ID,
			MYC_MESG.TO_USER_ID,
			MYC_MESG.TOFROM_PAT_C,
			MYC_MESG.PROXY_PAT_ID,
			MYC_MESG.WPR_OWNER_WPR_ID,
			MYC_MESG.OUTBOX_PAT_ID,
			MYC_MESG.INBOX_PAT_ID,
			MYC_MESG.PROV_ID,
			MYC_MESG.PAT_ENC_CSN_ID,
			MYC_MESG.INBASKET_MSG_ID,
			LEFT(y.msg_txts, LEN (y.msg_txts) - 1) AS msgs_txts
		FROM
			extern
			CROSS APPLY (
				SELECT
					msg_txt + ' ,'
				FROM
					dbo.MSG_TXT AS intern
				WHERE
					extern.MESSAGE_ID = intern.MESSAGE_ID FOR XML PATH(''), TYPE) x (msg_txts)
				CROSS APPLY (
					SELECT
						x.msg_txts.value ('(./text())[1]', 'NVARCHAR(MAX)')) y (msg_txts) --
				LEFT OUTER JOIN dbo.MYC_MESG MYC_MESG ON MYC_MESG.MESSAGE_ID = extern.MESSAGE_ID) TBL4 ON TBL1.PAT_ID = TBL4.myc_pat_id
	-- Provider and encouter joins
	LEFT JOIN (
		SELECT
			-- V_MYC_MESG columns
			v_myc_mesg.MYC_MSG_TYP_C, v_myc_mesg.MYC_MSG_TYP_DISPLAY, v_myc_mesg.MSG_SUBJECT, v_myc_mesg.SELECTED_RECIP_USER_ID, v_myc_mesg.SELECTED_RECIP_DISPLAY, v_myc_mesg.MESSAGE_ID AS v_myc_mesg_message_id, v_myc_mesg.INBASKET_MSG_ID, v_myc_mesg.FIRST_READ_IN_MYC_DTTM, v_myc_mesg.DAYS_FOR_PAT_TO_READ, v_myc_mesg.ENC_TYPE_C, v_myc_mesg.ENC_TYPE_DISPLAY, v_myc_mesg.PAT_ENC_CSN_ID, v_myc_mesg.EOW_READ_YN, v_myc_mesg.ENC_SERV_AREA_ID,
			-- CLARITY_SER columns
			clarity_ser.PROV_NAME, clarity_ser.PROV_ID, clarity_ser.PROV_TYPE, clarity_ser.IS_RESIDENT, clarity_ser.CLINICIAN_TITLE, clarity_ser.REFERRAL_SRCE_TYPE, clarity_ser.MCD_PROF_CD_C, clarity_ser.IS_VERIFIED_YN, clarity_ser.SER_REF_SRCE_ID, clarity_ser.ACTIVE_STATUS, clarity_ser.SEX, clarity_ser.BIRTH_DATE, clarity_ser.MEDS_AUTH_PROV_YN, clarity_ser.ORDS_AUTH_PROV_YN, clarity_ser.HOSPITALIST_YN, clarity_ser.DOCTORS_DEGREE, clarity_ser.PHARMACIST_YN, clarity_ser.EPIC_PROV_ID, clarity_ser.REFERRAL_SOURCE_TYPE_C, clarity_ser.REFERRAL_SOURCE_TYPE,
			-- Encounter department columns
			clarity_dep.DEPARTMENT_ID, clarity_dep.DEPARTMENT_NAME, clarity_dep.SPECIALTY AS prov_dep_specialty, clarity_dep.INPATIENT_DEPT_YN
		FROM
			DBO.V_MYC_MESG v_myc_mesg
			JOIN DBO.CLARITY_SER clarity_ser ON (v_myc_mesg.ENC_PROV_ID = clarity_ser.PROV_ID)
			LEFT JOIN DBO.CLARITY_DEP clarity_dep ON (v_myc_mesg.ENC_DEP_ID = clarity_dep.DEPARTMENT_ID)) TBL6 ON TBL6.v_myc_mesg_message_id = TBL4.myc_message_id
	---------------------------------------------------------------------------------------------
	-- TBL5: Columns giving relatives to teen patient
	---------------------------------------------------------------------------------------------
	LEFT JOIN (
		SELECT
			-- PAT_RELATIONSHIPS
			r1.pat_id AS relation_pat_id,
			r1.line AS relation_line,
			r1.pat_rel_name,
			r1.PAT_REL_RELATION_C,
			-- ZC_PAT_RELATION
			r2.title AS relation_title,
			r3.ProxyPatIDList
		FROM
			dbo.pat_relationships r1
			LEFT JOIN dbo.ZC_PAT_RELATION r2 ON (r1.PAT_REL_RELATION_C = r2.PAT_RELATION_C)
			LEFT JOIN proxy_pat_list r3 ON (r1.PAT_ID = r3.PAT_ID)) TBL5 ON TBL1.pat_id = TBL5.relation_pat_id
	---------------------------------------------------------------------------------------------
	-- Exclusion Criteria
	---------------------------------------------------------------------------------------------
WHERE
	TBL1.mychart_status_c = 1 -- 1: Activated, 2: Inactivated, 3: Pending Activation, 4: Non standard MYChart Status, 5: Patient Declined, 6: Activation Cod eGenerated, but disabled
	AND TBL2.login_name IS NOT NULL
	AND TBL3.birth_date BETWEEN '02/26/2002' AND '02/26/2008' --AND TBL3.birth_date BETWEEN '03/26/2002' AND  '03/26/2008' --  -- For research dataset
	AND TBL4.CREATED_TIME BETWEEN '02/26/2014' AND '02/26/2020' -- For research dataset. Message cannot be created before the child was at least 12.
